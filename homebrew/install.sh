#!/usr/bin/env bash

set -e

SOURCE_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

brew bundle --no-lock --file="${SOURCE_DIR}/Brewfile"
