#!/usr/bin/env bash

set -e

SOURCE_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

mkdir -p "${HOME}/.dotfiles"

rm -f "${HOME}/.dotfiles/bin"
ln -s "${SOURCE_DIR}/bin" "${HOME}/.dotfiles/bin"

"${SOURCE_DIR}/homebrew/install.sh"
"${SOURCE_DIR}/git/install.sh"
"${SOURCE_DIR}/ssh/install.sh"
"${SOURCE_DIR}/vim/install.sh"
"${SOURCE_DIR}/zsh/install.sh"
"${SOURCE_DIR}/gnupg/install.sh"
