#!/usr/bin/env bash

set -e

SOURCE_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

mkdir -p "${HOME}/.ssh"
chmod 700 "${HOME}/.ssh"

# create config
if [ ! -f "${HOME}/.ssh/config" ]; then
  touch "${HOME}/.ssh/config"
  chmod 600 "${HOME}/.ssh/config"
fi

config="$(sed '/^# BEGIN HEADER$/,/^# END HEADER$/d' "${HOME}/.ssh/config")"
echo -e "# BEGIN HEADER" >"${HOME}/.ssh/config"
cat "${SOURCE_DIR}/config" >>"${HOME}/.ssh/config"
if [[ "$OSTYPE" == "darwin"* ]]; then
  echo "UseKeychain yes" >>"${HOME}/.ssh/config"
fi
echo -e "# END HEADER" >>"${HOME}/.ssh/config"
echo "${config}" >>"${HOME}/.ssh/config"
