#!/usr/bin/env bash

set -e

SOURCE_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

mkdir -p "${HOME}/.gnupg"
chmod 700 "${HOME}/.gnupg"

cp "${SOURCE_DIR}/gpg-agent.conf" "${HOME}/.gnupg/gpg-agent.conf"

if [[ $(which pinentry-mac &>/dev/null && echo $?) -eq 0 ]]; then
  echo "pinentry-program $(which pinentry-mac)" >>"${HOME}/.gnupg/gpg-agent.conf"
fi
