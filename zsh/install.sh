#!/usr/bin/env bash

set -e

SOURCE_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

rm -f "${HOME}/.zshrc"
ln -s "${SOURCE_DIR}/zshrc" "${HOME}/.zshrc"

rm -f "${HOME}/.zprofile"
ln -s "${SOURCE_DIR}/zprofile" "${HOME}/.zprofile"

rm -f "${HOME}/.p10k.zsh"
ln -s "${SOURCE_DIR}/p10k.zsh" "${HOME}/.p10k.zsh"

mkdir -p "${HOME}/.zsh"

rm -f "${HOME}/.zsh/aliases"
ln -s "${SOURCE_DIR}/aliases" "${HOME}/.zsh/aliases"

rm -f "${HOME}/.zsh/completions"
ln -s "${SOURCE_DIR}/completions" "${HOME}/.zsh/completions"
