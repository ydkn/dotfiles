#!/usr/bin/env bash

set -e

SOURCE_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

rm -f "${HOME}/.gitignore"
ln -s "${SOURCE_DIR}/gitignore" "${HOME}/.gitignore"

if [ "$(uname)" == "Darwin" ]; then
  export CREDENTIAL_HELPER="osxkeychain"
else
  export CREDENTIAL_HELPER="store"
fi

rm -f "${HOME}/.gitconfig"
eval "$(which envsubst)" <"${SOURCE_DIR}/gitconfig" >"${HOME}/.gitconfig"

if [ ! -f "${HOME}/.gitconfig.local" ]; then
  touch "${HOME}/.gitconfig.local"
fi
