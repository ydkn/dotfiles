#!/usr/bin/env bash

set -e

SOURCE_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

rm -f "${HOME}/.vimrc"
ln -s "${SOURCE_DIR}/vimrc" "${HOME}/.vimrc"

mkdir -p "${HOME}/.vim"

rm -rf "${HOME}/.vim/colors"
ln -s "${SOURCE_DIR}/colors" "${HOME}/.vim/colors"

rm -rf "${HOME}/.vim/ftdetect"
ln -s "${SOURCE_DIR}/ftdetect" "${HOME}/.vim/ftdetect"

rm -rf "${HOME}/.vim/indent"
ln -s "${SOURCE_DIR}/indent" "${HOME}/.vim/indent"

rm -rf "${HOME}/.vim/plugin"
ln -s "${SOURCE_DIR}/plugin" "${HOME}/.vim/plugin"
